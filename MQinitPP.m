function [ioObj,address] = MQinitPP

%create IO32 interface object
ioObj = io32;

% check the port status
status = io32(ioObj);

if status == 0
    address = hex2dec('2020');
    display(['Yay, we have a functioning parallel port at: ' num2str(address)])
else
    display('Failed')
    ioObj = [];
end



