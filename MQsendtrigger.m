function MQsendtrigger(ioObj,address,triggerline,duration)

if ~exist('duration','var')
    duration = .005;
end

io32(ioObj,address,triggerline-128);
pause(duration)
io32(ioObj,address,0);

end

